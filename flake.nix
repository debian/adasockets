{
  description = "BSD sockets for Ada";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/master";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; };
      in {
        packages.default = pkgs.stdenv.mkDerivation {
          name = "adasockets";
          src = self;
          nativeBuildInputs = with pkgs; [
            autoreconfHook gnat autoconf automake libtool gnum4 texinfo
            texlive.combined.scheme-small autogen pre-commit makeWrapper
          ];
          configureFlags = [ "--disable-examples" "--disable-tests" ];
          postInstall = ''
            wrapProgram $out/bin/adasockets-config \
              --prefix PATH : ${pkgs.pkg-config}/bin \
              --prefix PKG_CONFIG_PATH : $out/lib/pkgconfig
          '';
        };
      });
}
